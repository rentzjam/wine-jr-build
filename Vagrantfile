# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2.2.0"

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
    (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

# Vagrant file for setting up a build environment for wine
# Based on https://github.com/ValveSoftware/Proton
if OS.linux?
  #cpus = `nproc`.to_i
  # meminfo shows KB and we need to convert to MB
  #memory = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
  cpus = 4
  memory = 2048
elsif OS.mac?
  #cpus = `sysctl -n hw.physicalcpu`.to_i
  # sysctl shows bytes and we need to convert to MB
  #memory = `sysctl hw.memsize | sed -e 's/hw.memsize: //'`.to_i / 1024 / 1024 / 4
  cpus = 4
  memory = 2048
else
  cpus = 4
  memory = 2048
  puts "Vagrant launched from unsupported platform."
end
#memory = [memory, 4096].max
puts "Platform: " + cpus.to_s + " CPUs, " + memory.to_s + " MB memory"

Vagrant.configure(2) do |config|
  #libvirt doesn't have a decent synced folder, so we have to use vagrant-sshfs.
  #This is not needed for virtualbox, but I couldn't find a way to use a
  #different synced folder type per provider, so we always use it.
  config.vagrant.plugins = "vagrant-sshfs"

  config.vm.provider "virtualbox" do |v|
    v.cpus = [cpus, 32].min     # virtualbox limit is 32 cpus
    v.memory = memory
  end

  config.vm.provider "libvirt" do |v|
    v.cpus = cpus
    v.memory = memory
    v.random_hostname = true
    v.default_prefix = ENV['USER'].to_s.dup.concat('_').concat(File.basename(Dir.pwd))
  end

$set_environment_variables = <<SCRIPT
tee "/etc/profile.d/myvars.sh" > "/dev/null" <<EOF

# Wine build env
export WINE_BUILD_JOB_LIMIT=4
export WINE_BUILD_BRANCH=#{ENV['WINE_BUILD_BRANCH']}
export WINE_BUILD_SOURCE_FOLDER=/home/vagrant/wine-jr/wine/git
export WINE_BUILD_FOLDER=/home/vagrant/wine-jr/wine/build
export WINE_BUILD_OUTPUT_FOLDER=/home/vagrant/wine-jr/wine/build/output
export WINE_BUILD_FINAL_OUTPUT_FOLDER=/vagrant/wine-jr/wine/output
EOF
SCRIPT

  #ubuntu2004-based build VM
  config.vm.box = "generic/ubuntu2004"
  config.vm.define "ubuntu2004", primary: true do |ubuntu2004|

    ubuntu2004.vm.box = "generic/ubuntu2004"

    ubuntu2004.vm.synced_folder "./vagrant_share/", "/vagrant/", create: true, type: "sshfs", sshfs_opts_append: "-o cache=no"
    ubuntu2004.vm.synced_folder ".", "/home/vagrant/wine-jr", id: "wine-jr", type: "rsync", rsync__exclude: ["vagrant_share"]

    ubuntu2004.vm.provision "shell", inline: $set_environment_variables, run: "always"

    ubuntu2004.vm.provision "shell", inline: <<-SHELL
      # Add 386 arch
      dpkg --add-architecture i386
      # Update package list.
      apt-get update
      # Do a basic update.
      apt-get -y upgrade
      apt-get -y dist-upgrade
      apt-get -y autoremove
      # Install all wine build depends.
      apt-get -y install build-essential gcc-multilib fakeroot devscripts dh-make gnupg gnupg2 wget most git gcc gcc-mingw-w64 ccache \
      autotools-dev autoconf bison bsdmainutils docbook-to-man docbook-utils docbook-xsl flex fontforge gawk gcc gettext patch perl sharutils \
      libacl1-dev libasound2-dev libcapi20-dev libcups2-dev libdbus-1-dev libfontconfig1-dev libfreetype6-dev libgl1-mesa-dev libglu1-mesa-dev \
      libgnutls28-dev libgphoto2-dev libgsm1-dev libgtk-3-dev libice-dev libjpeg-dev libkrb5-dev liblcms2-dev libldap2-dev libldap-dev libmpg123-dev \
      libncurses-dev libopenal-dev libosmesa6-dev libpcap-dev libpng-dev libpulse-dev libsane-dev libsdl2-dev libssl-dev libtiff-dev libudev-dev libv4l-dev \
      libx11-dev libxcomposite-dev libxcursor-dev libxext-dev libxi-dev libxinerama-dev libxml2-dev libxrandr-dev libxrender-dev libxslt1-dev libxt-dev \
      libxxf86vm-dev linux-libc-dev ocl-icd-opencl-dev unixodbc-dev x11proto-xinerama-dev \
      libacl1-dev:i386 libasound2-dev:i386 libcapi20-dev:i386 libcups2-dev:i386 libdbus-1-dev:i386 libfontconfig1-dev:i386 libfreetype6-dev:i386 libgl1-mesa-dev:i386 libglu1-mesa-dev:i386 \
      libgnutls28-dev:i386 libgphoto2-dev:i386 libgsm1-dev:i386 libgtk-3-dev:i386 libice-dev:i386 libjpeg-dev:i386 libkrb5-dev:i386 liblcms2-dev:i386 libldap2-dev:i386 libldap-dev:i386 libmpg123-dev:i386 \
      libncurses-dev:i386 libopenal-dev:i386 libosmesa6-dev:i386 libpcap-dev:i386 libpng-dev:i386 libpulse-dev:i386 libsane-dev:i386 libsdl2-dev:i386 libssl-dev:i386 libtiff-dev:i386 libudev-dev:i386 libv4l-dev:i386 \
      libx11-dev:i386 libxcomposite-dev:i386 libxcursor-dev:i386 libxext-dev:i386 libxi-dev:i386 libxinerama-dev:i386 libxml2-dev:i386 libxrandr-dev:i386 libxrender-dev:i386 libxslt1-dev:i386 libxt-dev:i386 \
      libxxf86vm-dev:i386 linux-libc-dev:i386 ocl-icd-opencl-dev:i386 unixodbc-dev:i386
    SHELL

  end

  
end
