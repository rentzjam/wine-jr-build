wine-jr - 
This contains a custom wine 6.7 build (based on 3ba4412be60dafee310b5d3c71aa762aa8846564) that applies a series of patches to enhance wine.
These patches provide the following features that are needed by certain applications.

Features:
* Setting system time.
* Setting timezone.
* Setting hostname.
* Custom support for reboot/poweroff either directly, scripts or just soft reboots.
* Fixes for wine/wineserver so that reboots will work correctly.
* Setting display resolution via custom means.
* Better serial port support
* Add some custom Windows functionality that is undocumented.
* Custom hooks for applications that rely on Windows functionality that is not practical to implement in Wine.
* Improve performance of some Windows applications that do bad Windows practices (load stuff in dllmain).

Some notes:
* sudo is required for setting the timezone, hostname since it uses systemd.
* wineserver needs cap_sys_ptrace=ep
* wine64-preloader, wine-preloader needs cap_sys_time=ep
* A lot of the "features" above are hidden behind registry settings since this stuff is rather custom.
So, any application that needs the functionality will need to setup the registry to use it.
It should be noted that the registry is used since Wine environment variables are not copied across reboots (tricky to fix).
That is why the registry is used.
* wine-staging or any wine patches that use secomp will break usage of sudo.

To build:
A Vagrantfile/env/setup is provided to build the wine build that has the custom changes.
Tested on Linux (Fedora 34+custom libvirt/kvm) and a Windows 10 KVM VM (Nested Virtualization).
It is known that some wine changes will not build on Apple.

Follow these steps to build:

1. Install Vagrant:
https://www.vagrantup.com/downloads

2. Install Virtual Box:
https://www.virtualbox.org/wiki/Downloads

NOTE: If you are performing the build in a VM then be certain that nested virtualization is enabled and working
for your virtualzation environment.  VirtualBox will need it.

3. Make sure you have plenty of diskspace for the VM and source.

4. Edit the Vagrantfile for your needs.  This could include increasing the number of cpus and/or memory.
The WINE_BUILD_JOB_LIMIT could be tweaked to match the cpu limits if desired.

5. Set WINE_BUILD_BRANCH environment variable to the correct branch if desired.
This is not 100% required since the latest tag should be pulled that is the correct version but included.  

6. Bring up a terminal for your host operating system.  All builds are done via the command line.
   So, either a simple cmd prompt on Windows or a bash terminal on a Linux environment.

7. Clone this project and submodules:
   Type -> git clone --recurse-submodules URL

8. Bring up vagrant:
   Type -> vagrant up

9. Wait a while for the vagrant VM environment to setup.  This will include installing the base Ubuntu 20.04 environment and all wine build depends.

10. Perform the real build:
   Type -> vagrant ssh -c '$WINE_BUILD_FOLDER/pbuild.sh'

11. The build process will now be performed in the VM.  This will take a while.
Platform: 4 CPUs, 2048 MB memory
2021-08-24T00:46:43.034UTC - Build Starting for wine-6.7+.jr.1
2021-08-24T00:46:43.034UTC - Cleaning
2021-08-24T00:46:43.048UTC - Configuring
2021-08-24T00:46:43.049UTC - Configuring wine64
2021-08-24T00:47:02.904UTC - Configuring wine32
2021-08-24T00:47:19.641UTC - Building
2021-08-24T00:47:19.642UTC - Building wine64
2021-08-24T00:47:51.700UTC - Building wine32
2021-08-24T00:48:25.793UTC - Installing wine32
2021-08-24T00:48:49.545UTC - Installing wine64
2021-08-24T00:49:13.481UTC - Creating archive
2021-08-24T00:50:38.370UTC - Success at build
2021-08-24T00:50:38.370UTC - Built archive file at /vagrant/wine-jr/wine/output/wine-6.7+.jr.1.tar.xz

13. Exit vagrant:
    Type -> vagrant halt.

NOTE: If you want to make wine-jr/wine changes then commit, etc.  Perform the same process again to vagrant up/build.
The existing VM will be used.

