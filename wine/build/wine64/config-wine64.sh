#!/bin/bash
# NOTE: disable gstreamer, usb (libusb) and gcrypt do not work 100% in cross compile.
# Now they WOULD work if a chroot is used if required...However, that requires 2 32bit build steps in a chroot/container.
# See: https://wiki.winehq.org/Building_Wine#Shared_WoW64

if [[ $(grep -i gcrypt $WINE_BUILD_SOURCE_FOLDER/configure -wc) -gt 0 ]] ;
then
        DISABLE_GCRYPT=--without-gcrypt
else
        DISABLE_GCRYPT=
fi

# Normal x86_64 build.
$WINE_BUILD_SOURCE_FOLDER/configure CC="ccache gcc" CROSSCC="ccache x86_64-w64-mingw32-gcc" --enable-win64 --prefix=$WINE_BUILD_OUTPUT_FOLDER --disable-tests --disable-win16 --without-gstreamer --without-usb --without-vkd3d --without-vulkan --without-jxrlib --without-faudio --without-oss --without-hal $DISABLE_GCRYPT
