#!/bin/bash
# NOTE: disable gstreamer, usb (libusb) and gcrypt do not work 100% in cross compile.
# Now they WOULD work if a chroot is used if required...However, that requires 2 32bit build steps in a chroot/container.
# See: https://wiki.winehq.org/Building_Wine#Shared_WoW64

if [[ $(grep -i gcrypt $WINE_BUILD_SOURCE_FOLDER/configure -wc) -gt 0 ]] ;
then
        DISABLE_GCRYPT=--without-gcrypt
else
        DISABLE_GCRYPT=
fi

# Do x86 cross compile and disable options for libraries that are not needed.
PKG_CONFIG_PATH=/usr/lib/pkgconfig $WINE_BUILD_SOURCE_FOLDER/configure CC="ccache gcc" CROSSCC="ccache i686-w64-mingw32-gcc" --with-wine64=$WINE_BUILD_FOLDER/wine64 --prefix=$WINE_BUILD_OUTPUT_FOLDER --disable-tests --disable-win16 --without-gstreamer --without-usb --without-vkd3d --without-vulkan --without-jxrlib --without-faudio --without-oss --without-hal $DISABLE_GCRYPT
