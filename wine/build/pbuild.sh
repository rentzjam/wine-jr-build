#!/bin/bash

export WINE_BUILD_OUTPUT_FOLDER_BASE=$WINE_BUILD_OUTPUT_FOLDER

mkdir -p $WINE_BUILD_OUTPUT_FOLDER_BASE

if [[ ! -z "$WINE_BUILD_BRANCH" ]]; then

    # Be sure to move to the correct branch.
    THE_CURRENT_DIR=$PWD

    echo "Using branch $WINE_BUILD_BRANCH."

    cd $WINE_BUILD_SOURCE_FOLDER

    if [[ $(git branch --show-current) != "$WINE_BUILD_BRANCH" ]];
    then
        git checkout $WINE_BUILD_BRANCH &>/dev/null
    fi

    if [[ $(git branch --show-current) != "$WINE_BUILD_BRANCH" ]];
    then
        cd $THE_CURRENT_DIR
        echo "Could not change to branch $WINE_BUILD_BRANCH."
        exit 1
    fi

    cd $THE_CURRENT_DIR
fi

BUILD_VERSION=$(cd $WINE_BUILD_SOURCE_FOLDER && git tag -l --sort=-creatordate | head -1)

if [[ -z $BUILD_VERSION ]]; 
then
    BUILD_VERSION=$(cd $WINE_BUILD_SOURCE_FOLDER && git rev-parse HEAD)
fi

LOG_FILE=$WINE_BUILD_OUTPUT_FOLDER_BASE/$BUILD_VERSION.build.log

export WINE_BUILD_OUTPUT_FOLDER=$WINE_BUILD_OUTPUT_FOLDER/$BUILD_VERSION

rm -f -R $WINE_BUILD_OUTPUT_FOLDER &>/dev/null

rm -f -R $WINE_BUILD_FINAL_OUTPUT_FOLDER &>/dev/null

mkdir -p $WINE_BUILD_OUTPUT_FOLDER

mkdir -p $WINE_BUILD_FINAL_OUTPUT_FOLDER

rm -f $LOG_FILE &>/dev/null

touch $LOG_FILE

log() { printf "%s\n" "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') - $*"; }

log "Build Starting for $BUILD_VERSION"

log "Cleaning"

# Clean existing projects.
rm -f -R $WINE_BUILD_OUTPUT_FOLDER &>/dev/null

(cd $WINE_BUILD_FOLDER/wine64 && ./clean.sh &>>$LOG_FILE)
(cd $WINE_BUILD_FOLDER/wine32 && ./clean.sh &>>$LOG_FILE)

log "Configuring"

# Perform configure.

log "Configuring wine64"
(cd $WINE_BUILD_FOLDER/wine64 && ./config-wine64.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when configuring wine64."
    exit 1
fi

log "Configuring wine32"
(cd $WINE_BUILD_FOLDER/wine32 && ./config-wine32.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when configuring wine32."
    exit 1
fi

log "Building"

# Build

log "Building wine64"

(cd $WINE_BUILD_FOLDER/wine64 && ./build.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when building wine64."
    exit 1
fi

log "Building wine32"
(cd $WINE_BUILD_FOLDER/wine32 && ./build.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when buildling wine32."
    exit 1
fi

# Install

log "Installing wine32"

(cd $WINE_BUILD_FOLDER/wine32 && ./build-install.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when installing wine32."
    exit 1
fi

log "Installing wine64"

(cd $WINE_BUILD_FOLDER/wine64 && ./build-install.sh &>>$LOG_FILE)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when installing wine64."
    exit 1
fi

rm -f $WINE_BUILD_OUTPUT_FOLDER_BASE/$BUILD_VERSION.tar.xz &>/dev/null

log "Creating archive"
# Create an archive.
(cd $WINE_BUILD_OUTPUT_FOLDER && tar -cJf $WINE_BUILD_OUTPUT_FOLDER_BASE/$BUILD_VERSION.tar.xz * && cd - &>/dev/null)

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when creating archive."
    exit 1
fi

# Copy archive and log files to final build output.
cp -f -ax $WINE_BUILD_OUTPUT_FOLDER_BASE/$BUILD_VERSION.tar.xz $WINE_BUILD_FINAL_OUTPUT_FOLDER &>>$LOG_FILE

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when copying archive ($WINE_BUILD_OUTPUT_FOLDER_BASE/$BUILD_VERSION.tar.xz) to $WINE_BUILD_FINAL_OUTPUT_FOLDER. See the log file at $LOG_FILE for more information."
    exit 1
fi

rm -f -R $WINE_BUILD_FINAL_OUTPUT_FOLDER/current_version.txt &>/dev/null

echo $BUILD_VERSION > $WINE_BUILD_FINAL_OUTPUT_FOLDER/current_version.txt

cp -f -ax $LOG_FILE $WINE_BUILD_FINAL_OUTPUT_FOLDER &>/dev/null

last_exe_error=$?

if [[ $last_exe_error -ne 0 ]];
then
    log "An error occurred when copying log file ($LOG_FILE) to $WINE_BUILD_FINAL_OUTPUT_FOLDER. See the log file at $LOG_FILE for more information."
    exit 1
fi

log "Success at build"
log "Built archive file at $WINE_BUILD_FINAL_OUTPUT_FOLDER/$BUILD_VERSION.tar.xz"
exit 0
